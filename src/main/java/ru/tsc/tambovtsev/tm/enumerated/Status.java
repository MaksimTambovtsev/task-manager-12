package ru.tsc.tambovtsev.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

}