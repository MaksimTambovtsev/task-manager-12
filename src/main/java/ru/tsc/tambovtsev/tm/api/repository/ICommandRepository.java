package ru.tsc.tambovtsev.tm.api.repository;

import ru.tsc.tambovtsev.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
